import unittest

from snet.node import Node
from snet.msg import Msg


class XNode(Node):
    def recive_msg(self, msg):
        print msg
        assert msg.get_payload() == 'Hello world!'


class MainT(unittest.TestCase):
    def test_main(self):
        root = Node('root')
        a = XNode('a')
        b = XNode('b')
        a.connect(root)
        b.connect(root)
        msg = Msg('a:root:b', 'Hello world!')
        msg.send(a)


if __name__ == '__main__':
    unittest.main()
