# from twisted.internet.base import DelayedCall
# DelayedCall.debug = True

from twisted.trial import unittest
from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks, Deferred

from snet.twisted_node import TwNode
from snet.msg import Msg


class XNode(TwNode):
    def recive_msg(self, msg):
        print '>>', msg
        assert msg.get_payload() == 'Hello world!'


class MainT(unittest.TestCase):
    @inlineCallbacks
    def test_main(self):
        root = TwNode('root', 7101)
        a = XNode('a', 7102)
        b = XNode('b', 7103)

        reactor.callLater(1, lambda: a.connect('root', 7101))
        reactor.callLater(2, lambda: b.connect('root', 7101))
        msg = Msg('a:root:b', 'Hello world!')
        reactor.callLater(3, lambda: msg.send(a))

        reactor.callLater(4, lambda: a.stop())
        reactor.callLater(4, lambda: b.stop())
        reactor.callLater(4, lambda: root.stop())

        d = Deferred()
        reactor.callLater(5, d.callback, None)
        yield d


if __name__ == '__main__':
    unittest.main()
