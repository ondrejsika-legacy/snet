class SnetError(Exception):
    pass


class NodeError(SnetError):
    pass

class AddressNotFoundError(NodeError):
    pass

class AddressColisionError(NodeError):
    pass
