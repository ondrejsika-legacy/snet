from .errors import AddressColisionError, AddressNotFoundError

class Node(object):
    _connected = None
    _name = None

    def __init__(self, name):
        self._name = name
        self._connected = {}

    def __repr__(self):
        return '<Snet:Node: %s %s>' % (
            self._name,
            self.get_connections(),
        )

    def get_name(self):
        return self._name

    def get_connected(self):
        return self._connected

    def get_connections(self):
        return len(self._connected)

    def connect(self, node):
        node.connect_apply(self)
        self.connect_apply(node)

    def connect_apply(self, node):
        if node.get_name() in self.get_connected():
            raise AddressColisionError()
        self._connected[node.get_name()] = node

    def transfer_msg(self, msg):
        next_node_name = msg.get_next_node_name()
        if next_node_name not in self._connected:
            raise AddressNotFoundError()
        msg.send(self._connected[next_node_name])

    def recive_msg(self, msg):
        raise NotImplementedError()

