import json

from twisted.internet import reactor
from twisted.internet.protocol import Protocol, ClientFactory

from .errors import AddressNotFoundError
from .node import Node
from .msg import Msg

class P(Protocol):
    def __init__(self, conn_req=None):
        self._conn_req = conn_req

    def connectionMade(self):
        if self._conn_req:
            target, name = self._conn_req
            self.transport.write(json.dumps(['CONNECT', [name, ]]))
            self.node._connected[target] = self

    def dataReceived(self, data):
        type, payload = json.loads(data)
        if type == 'CONNECT':
            name, = payload
            self.node._connected[name] = self
        elif type == 'MSG':
            msg = Msg.deserialize(payload)
            msg.send(self.node)


class TwNode(Node, ClientFactory):
    _port = None
    _listener = None

    def __init__(self, name, port):
        super(TwNode, self).__init__(name)
        self._port = port
        self._conn_req = None
        self._listener = reactor.listenTCP(self._port, self)

    def __repr__(self):
        return '<Snet:Node(Tw): %s %s>' % (
            self._name,
            self.get_connections(),
        )

    def stop(self):
        for conn in self._connected.values():
            conn.transport.loseConnection()
        self._listener.stopListening()

    def get_connected(self):
        return self._connected

    def get_connections(self):
        return len(self._connected)

    def connect(self, name, port):
        self._conn_req = name, self._name
        reactor.connectTCP('localhost', port, self)

    def transfer_msg(self, msg):
        next_node_name = msg.get_next_node_name()
        if next_node_name not in self._connected:
            raise AddressNotFoundError()
        self._connected[next_node_name].transport.write(json.dumps(['MSG', msg.serialize()]))

    def recive_msg(self, msg):
        raise NotImplementedError()

    # Factory
    def buildProtocol(self, addr):
        p = P(self._conn_req)
        p.node = self
        return p
