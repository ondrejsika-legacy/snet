from .errors import AddressNotFoundError

class TwMsg(object):
    _link_name = None
    _link = None
    _payload = None
    _target = None

    def __init__(self, link_name, payload):
        self._link_name = link_name
        self._link = self._link_name.split(':')
        self._payload = payload
        self._target = 0

    def __repr__(self):
        return '<Snet:Msg(Tw): %s %s %s>' % (
            self._link_name,
            self._target,
            str(self._payload)[:16],
        )

    def get_link_name(self):
        return self._link_name

    def get_link(self):
        return self._link

    def get_next_node_name(self):
        return self._link[self._target]

    def get_payload(self):
        return self._payload

    def send(self, node):
        """
        Send message to specific node.
        """
        if self.get_next_node_name() != node.get_name():
            raise AddressNotFoundError()
        self._target += 1
        if self._target == len(self._link):
            node.recive_msg(self)
        else:
            node.transfer_msg(self)
